# 宝宝速记-停更新版请移步到 https://gitee.com/dqwangyang/baby-shorthand-prod

#### 介绍
 **此仓库已经不在维护，最新版请移步**  
https://gitee.com/dqwangyang/baby-shorthand-prod

婴儿日常行为记录，比如喂奶，辅食，便便，睡觉等

#### 软件架构

1. 本项目采用小程序的云开发，代码只是给出了业务代码，导入IDE后并不能直接运行
1. 采用vant weapp 地址:https://youzan.github.io/vant-weapp/#/intro



#### 安装教程

1.  下载代码
2.  自己先创建个云开发项目
3.  把相关的代码页面拷贝到刚才自己创建的小程序项目下

#### 使用说明
截图如下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0817/104509_5860192c_933825.jpeg "b1.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0817/113209_fe1d34b9_933825.jpeg "n.jpg")



扫码体验
![扫码体验](https://images.gitee.com/uploads/images/2020/0817/104541_42aecf45_933825.png "微信图片_20200810181631.png")


 代码写的比较辣鸡 :joy: 大佬们凑合着看吧，！
